import { BigNumber } from 'bignumber.js';
// Siacoin -> hastings unit conversion functions
// These make conversion between units of Sia easy and consistent for developers.
// Never return exponentials from BigNumber.toString, since they confuse the API
BigNumber.config({ EXPONENTIAL_AT: 1e9 });
BigNumber.config({ DECIMAL_PLACES: 30 });
// Hastings is the lowest divisible unit in Sia. This constant will be used to
// calculate the conversion between the base unit to human readable values.
const hastingsPerSiacoin = new BigNumber('10').exponentiatedBy(24);
export function toSiacoins(hastings) {
    return new BigNumber(hastings).dividedBy(hastingsPerSiacoin);
}
export function toHastings(siacoins) {
    return new BigNumber(siacoins).times(hastingsPerSiacoin);
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY3VycmVuY3kuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyIuLi8uLi8uLi9zcmMvbGliL2N1cnJlbmN5LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBLE9BQU8sRUFBRSxTQUFTLEVBQUUsTUFBTSxjQUFjLENBQUM7QUFFekMsZ0RBQWdEO0FBQ2hELGlGQUFpRjtBQUNqRixnRkFBZ0Y7QUFDaEYsU0FBUyxDQUFDLE1BQU0sQ0FBQyxFQUFFLGNBQWMsRUFBRSxHQUFHLEVBQUUsQ0FBQyxDQUFDO0FBQzFDLFNBQVMsQ0FBQyxNQUFNLENBQUMsRUFBRSxjQUFjLEVBQUUsRUFBRSxFQUFFLENBQUMsQ0FBQztBQUV6Qyw4RUFBOEU7QUFDOUUsMkVBQTJFO0FBQzNFLE1BQU0sa0JBQWtCLEdBQUcsSUFBSSxTQUFTLENBQUMsSUFBSSxDQUFDLENBQUMsZUFBZSxDQUFDLEVBQUUsQ0FBQyxDQUFDO0FBRW5FLE1BQU0sVUFBVSxVQUFVLENBQUMsUUFBNEI7SUFDckQsT0FBTyxJQUFJLFNBQVMsQ0FBQyxRQUFRLENBQUMsQ0FBQyxTQUFTLENBQUMsa0JBQWtCLENBQUMsQ0FBQztBQUMvRCxDQUFDO0FBRUQsTUFBTSxVQUFVLFVBQVUsQ0FBQyxRQUE0QjtJQUNyRCxPQUFPLElBQUksU0FBUyxDQUFDLFFBQVEsQ0FBQyxDQUFDLEtBQUssQ0FBQyxrQkFBa0IsQ0FBQyxDQUFDO0FBQzNELENBQUMifQ==