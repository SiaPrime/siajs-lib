"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const ava_1 = __importDefault(require("ava"));
const bignumber_js_1 = __importDefault(require("bignumber.js"));
const currency_1 = require("./currency");
const HASTINGS_PER_SIACOIN = '1000000000000000000000000';
ava_1.default('converts from siacoins to hastings correctly', t => {
    const maxSC = new bignumber_js_1.default('100000000000000000000000');
    for (let i = 0; i < 100; i++) {
        const sc = maxSC.times(Math.trunc(Math.random() * 10000) / 10000);
        const expectedHastings = sc.times(HASTINGS_PER_SIACOIN);
        t.is(currency_1.toHastings(sc).toString(), expectedHastings.toString());
    }
});
ava_1.default('converts from hastings to siacoins correctly', t => {
    const maxH = new bignumber_js_1.default('10').pow(150);
    for (let i = 0; i < 100; i++) {
        const hastings = maxH.times(Math.trunc(Math.random() * 10000) / 10000);
        const expectedSiacoins = hastings.dividedBy(HASTINGS_PER_SIACOIN);
        t.is(currency_1.toSiacoins(hastings).toString(), expectedSiacoins.toString());
    }
});
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY3VycmVuY3kuc3BlYy5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbIi4uLy4uLy4uL3NyYy9saWIvY3VycmVuY3kuc3BlYy50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7OztBQUFBLDhDQUF1QjtBQUN2QixnRUFBcUM7QUFDckMseUNBQW9EO0FBRXBELE1BQU0sb0JBQW9CLEdBQUcsMkJBQTJCLENBQUM7QUFFekQsYUFBSSxDQUFDLDhDQUE4QyxFQUFFLENBQUMsQ0FBQyxFQUFFO0lBQ3ZELE1BQU0sS0FBSyxHQUFHLElBQUksc0JBQVMsQ0FBQywwQkFBMEIsQ0FBQyxDQUFDO0lBQ3hELEtBQUssSUFBSSxDQUFDLEdBQUcsQ0FBQyxFQUFFLENBQUMsR0FBRyxHQUFHLEVBQUUsQ0FBQyxFQUFFLEVBQUU7UUFDNUIsTUFBTSxFQUFFLEdBQUcsS0FBSyxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxNQUFNLEVBQUUsR0FBRyxLQUFLLENBQUMsR0FBRyxLQUFLLENBQUMsQ0FBQztRQUNsRSxNQUFNLGdCQUFnQixHQUFHLEVBQUUsQ0FBQyxLQUFLLENBQUMsb0JBQW9CLENBQUMsQ0FBQztRQUN4RCxDQUFDLENBQUMsRUFBRSxDQUFDLHFCQUFVLENBQUMsRUFBRSxDQUFDLENBQUMsUUFBUSxFQUFFLEVBQUUsZ0JBQWdCLENBQUMsUUFBUSxFQUFFLENBQUMsQ0FBQztLQUM5RDtBQUNILENBQUMsQ0FBQyxDQUFDO0FBRUgsYUFBSSxDQUFDLDhDQUE4QyxFQUFFLENBQUMsQ0FBQyxFQUFFO0lBQ3ZELE1BQU0sSUFBSSxHQUFHLElBQUksc0JBQVMsQ0FBQyxJQUFJLENBQUMsQ0FBQyxHQUFHLENBQUMsR0FBRyxDQUFDLENBQUM7SUFDMUMsS0FBSyxJQUFJLENBQUMsR0FBRyxDQUFDLEVBQUUsQ0FBQyxHQUFHLEdBQUcsRUFBRSxDQUFDLEVBQUUsRUFBRTtRQUM1QixNQUFNLFFBQVEsR0FBRyxJQUFJLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDLE1BQU0sRUFBRSxHQUFHLEtBQUssQ0FBQyxHQUFHLEtBQUssQ0FBQyxDQUFDO1FBQ3ZFLE1BQU0sZ0JBQWdCLEdBQUcsUUFBUSxDQUFDLFNBQVMsQ0FBQyxvQkFBb0IsQ0FBQyxDQUFDO1FBQ2xFLENBQUMsQ0FBQyxFQUFFLENBQUMscUJBQVUsQ0FBQyxRQUFRLENBQUMsQ0FBQyxRQUFRLEVBQUUsRUFBRSxnQkFBZ0IsQ0FBQyxRQUFRLEVBQUUsQ0FBQyxDQUFDO0tBQ3BFO0FBQ0gsQ0FBQyxDQUFDLENBQUMifQ==