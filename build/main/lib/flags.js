"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const utils_1 = require("./utils");
/**
 * Creates a list of flags ready to be consumed by the spawn process.
 * @param config
 */
function parseFlags(config) {
    // Default flags used in the Sia Daemon.
    const defaultFlags = {
        'api-addr': 'localhost:4280',
        'authenticate-api': false,
        'disable-api-security': false,
        'host-addr': ':4282',
        'rpc-addr': ':4281'
    };
    // Build flag arguements from constructor details
    const flags = utils_1.assignDefined(defaultFlags, {
        agent: config.agent,
        'api-addr': config.apiHost && config.apiPort
            ? `${config.apiHost}:${config.apiPort}`
            : null,
        // If auto, we are going to attempt to resolve api password through the default sia file.
        'authenticate-api': config.apiAuthentication === 'auto' ? null : config.apiAuthentication,
        'host-addr': config.hostPort ? `:${config.hostPort}` : null,
        modules: config.modules ? parseModules(config.modules) : null,
        'rpc-addr': config.rpcPort ? `:${config.rpcPort}` : null,
        'scprime-directory': config.dataDirectory,
        'temp-password': config.apiAuthenticationPassword
    });
    // Create flag string list to pass to siad
    const filterFlags = (key) => flags[key] !== false;
    const mapFlags = (key) => `--${key}=${flags[key]}`;
    const flagList = Object.keys(flags)
        .filter(filterFlags)
        .map(mapFlags);
    return flagList;
}
exports.parseFlags = parseFlags;
/**
 * Returns a module string that can be passed as a flag value to siad.
 * @param modules
 */
function parseModules(modules) {
    const moduleMap = {
        consensus: 'c',
        explorer: 'e',
        gateway: 'g',
        host: 'h',
        miner: 'm',
        renter: 'r',
        transactionPool: 't',
        wallet: 'w'
    };
    let enabledModules = '';
    Object.keys(modules).forEach(k => {
        if (modules[k]) {
            enabledModules += moduleMap[k];
        }
    });
    return enabledModules;
}
exports.parseModules = parseModules;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZmxhZ3MuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyIuLi8uLi8uLi9zcmMvbGliL2ZsYWdzLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7O0FBQ0EsbUNBQXdDO0FBRXhDOzs7R0FHRztBQUNILFNBQWdCLFVBQVUsQ0FBQyxNQUFvQjtJQUM3Qyx3Q0FBd0M7SUFDeEMsTUFBTSxZQUFZLEdBQWM7UUFDOUIsVUFBVSxFQUFFLGdCQUFnQjtRQUM1QixrQkFBa0IsRUFBRSxLQUFLO1FBQ3pCLHNCQUFzQixFQUFFLEtBQUs7UUFDN0IsV0FBVyxFQUFFLE9BQU87UUFDcEIsVUFBVSxFQUFFLE9BQU87S0FDcEIsQ0FBQztJQUNGLGlEQUFpRDtJQUNqRCxNQUFNLEtBQUssR0FBRyxxQkFBYSxDQUFDLFlBQVksRUFBRTtRQUN4QyxLQUFLLEVBQUUsTUFBTSxDQUFDLEtBQUs7UUFDbkIsVUFBVSxFQUNSLE1BQU0sQ0FBQyxPQUFPLElBQUksTUFBTSxDQUFDLE9BQU87WUFDOUIsQ0FBQyxDQUFDLEdBQUcsTUFBTSxDQUFDLE9BQU8sSUFBSSxNQUFNLENBQUMsT0FBTyxFQUFFO1lBQ3ZDLENBQUMsQ0FBQyxJQUFJO1FBQ1YseUZBQXlGO1FBQ3pGLGtCQUFrQixFQUNoQixNQUFNLENBQUMsaUJBQWlCLEtBQUssTUFBTSxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDLE1BQU0sQ0FBQyxpQkFBaUI7UUFDdkUsV0FBVyxFQUFFLE1BQU0sQ0FBQyxRQUFRLENBQUMsQ0FBQyxDQUFDLElBQUksTUFBTSxDQUFDLFFBQVEsRUFBRSxDQUFDLENBQUMsQ0FBQyxJQUFJO1FBQzNELE9BQU8sRUFBRSxNQUFNLENBQUMsT0FBTyxDQUFDLENBQUMsQ0FBQyxZQUFZLENBQUMsTUFBTSxDQUFDLE9BQU8sQ0FBQyxDQUFDLENBQUMsQ0FBQyxJQUFJO1FBQzdELFVBQVUsRUFBRSxNQUFNLENBQUMsT0FBTyxDQUFDLENBQUMsQ0FBQyxJQUFJLE1BQU0sQ0FBQyxPQUFPLEVBQUUsQ0FBQyxDQUFDLENBQUMsSUFBSTtRQUN4RCxtQkFBbUIsRUFBRSxNQUFNLENBQUMsYUFBYTtRQUN6QyxlQUFlLEVBQUUsTUFBTSxDQUFDLHlCQUF5QjtLQUNsRCxDQUFDLENBQUM7SUFDSCwwQ0FBMEM7SUFDMUMsTUFBTSxXQUFXLEdBQUcsQ0FBQyxHQUFXLEVBQUUsRUFBRSxDQUFDLEtBQUssQ0FBQyxHQUFHLENBQUMsS0FBSyxLQUFLLENBQUM7SUFDMUQsTUFBTSxRQUFRLEdBQUcsQ0FBQyxHQUFXLEVBQUUsRUFBRSxDQUFDLEtBQUssR0FBRyxJQUFJLEtBQUssQ0FBQyxHQUFHLENBQUMsRUFBRSxDQUFDO0lBQzNELE1BQU0sUUFBUSxHQUFHLE1BQU0sQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDO1NBQ2hDLE1BQU0sQ0FBQyxXQUFXLENBQUM7U0FDbkIsR0FBRyxDQUFDLFFBQVEsQ0FBQyxDQUFDO0lBQ2pCLE9BQU8sUUFBUSxDQUFDO0FBQ2xCLENBQUM7QUFoQ0QsZ0NBZ0NDO0FBRUQ7OztHQUdHO0FBQ0gsU0FBZ0IsWUFBWSxDQUFDLE9BQXFCO0lBQ2hELE1BQU0sU0FBUyxHQUFHO1FBQ2hCLFNBQVMsRUFBRSxHQUFHO1FBQ2QsUUFBUSxFQUFFLEdBQUc7UUFDYixPQUFPLEVBQUUsR0FBRztRQUNaLElBQUksRUFBRSxHQUFHO1FBQ1QsS0FBSyxFQUFFLEdBQUc7UUFDVixNQUFNLEVBQUUsR0FBRztRQUNYLGVBQWUsRUFBRSxHQUFHO1FBQ3BCLE1BQU0sRUFBRSxHQUFHO0tBQ1osQ0FBQztJQUVGLElBQUksY0FBYyxHQUFHLEVBQUUsQ0FBQztJQUN4QixNQUFNLENBQUMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxDQUFDLE9BQU8sQ0FBQyxDQUFDLENBQUMsRUFBRTtRQUMvQixJQUFJLE9BQU8sQ0FBQyxDQUFDLENBQUMsRUFBRTtZQUNkLGNBQWMsSUFBSSxTQUFTLENBQUMsQ0FBQyxDQUFDLENBQUM7U0FDaEM7SUFDSCxDQUFDLENBQUMsQ0FBQztJQUNILE9BQU8sY0FBYyxDQUFDO0FBQ3hCLENBQUM7QUFuQkQsb0NBbUJDIn0=