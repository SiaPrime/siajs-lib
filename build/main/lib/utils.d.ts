/**
 * Works similarly to Object.assign, but checks properties for undefined or
 * null values, skipping them if detected.
 * @param target
 * @param sources
 */
export declare function assignDefined(target: object, ...sources: object[]): object;
/**
 * Retrieves the API password using the SIA_API_PASSWORD env variable or
 * attempt to read the local dir with fs.
 */
export declare function getSiaPassword(): string;
